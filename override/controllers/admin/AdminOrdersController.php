<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Order $object
 */

class AdminOrdersController extends AdminOrdersControllerCore
{
    public function renderView()
    {
        $order = new Order(Tools::getValue('id_order'));
        if (!Validate::isLoadedObject($order)) {
            $this->errors[] = Tools::displayError('The order cannot be found within your database.');
        }

        $customer = new Customer($order->id_customer);
        $carrier = new Carrier($order->id_carrier);
        $products = $this->getProducts($order);
        $currency = new Currency((int)$order->id_currency);
        // Carrier module call
        $carrier_module_call = null;
        if ($carrier->is_module) {
            $module = Module::getInstanceByName($carrier->external_module_name);
            if (method_exists($module, 'displayInfoByCart')) {
                $carrier_module_call = call_user_func(array($module, 'displayInfoByCart'), $order->id_cart);
            }
        }

        // Retrieve addresses information
        $addressInvoice = new Address($order->id_address_invoice, $this->context->language->id);
        if (Validate::isLoadedObject($addressInvoice) && $addressInvoice->id_state) {
            $invoiceState = new State((int)$addressInvoice->id_state);
        }

        if ($order->id_address_invoice == $order->id_address_delivery) {
            $addressDelivery = $addressInvoice;
            if (isset($invoiceState)) {
                $deliveryState = $invoiceState;
            }
        } else {
            $addressDelivery = new Address($order->id_address_delivery, $this->context->language->id);
            if (Validate::isLoadedObject($addressDelivery) && $addressDelivery->id_state) {
                $deliveryState = new State((int)($addressDelivery->id_state));
            }
        }

        $this->toolbar_title = sprintf($this->l('Order #%1$d (%2$s) - %3$s %4$s'), $order->id, $order->reference, $customer->firstname, $customer->lastname);
        if (Shop::isFeatureActive()) {
            $shop = new Shop((int)$order->id_shop);
            $this->toolbar_title .= ' - '.sprintf($this->l('Shop: %s'), $shop->name);
        }

        // gets warehouses to ship products, if and only if advanced stock management is activated
        $warehouse_list = null;

        $order_details = $order->getOrderDetailList();
        foreach ($order_details as $order_detail) {
            $product = new Product($order_detail['product_id']);

            if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')
                && $product->advanced_stock_management) {
                $warehouses = Warehouse::getWarehousesByProductId($order_detail['product_id'], $order_detail['product_attribute_id']);
                foreach ($warehouses as $warehouse) {
                    if (!isset($warehouse_list[$warehouse['id_warehouse']])) {
                        $warehouse_list[$warehouse['id_warehouse']] = $warehouse;
                    }
                }
            }
        }

        $payment_methods = array();
        foreach (PaymentModule::getInstalledPaymentModules() as $payment) {
            $module = Module::getInstanceByName($payment['name']);
            if (Validate::isLoadedObject($module) && $module->active) {
                $payment_methods[] = $module->displayName;
            }
        }

        // display warning if there are products out of stock
        $display_out_of_stock_warning = false;
        $current_order_state = $order->getCurrentOrderState();
        if (Configuration::get('PS_STOCK_MANAGEMENT') && (!Validate::isLoadedObject($current_order_state) || ($current_order_state->delivery != 1 && $current_order_state->shipped != 1))) {
            $display_out_of_stock_warning = true;
        }

        // products current stock (from stock_available)
        foreach ($products as &$product) {
            // Get total customized quantity for current product
            $customized_product_quantity = 0;

            if (is_array($product['customizedDatas'])) {
                foreach ($product['customizedDatas'] as $customizationPerAddress) {
                    foreach ($customizationPerAddress as $customizationId => $customization) {
                        $customized_product_quantity += (int)$customization['quantity'];
                    }
                }
            }

            $product['customized_product_quantity'] = $customized_product_quantity;
            $product['current_stock'] = StockAvailable::getQuantityAvailableByProduct($product['product_id'], $product['product_attribute_id'], $product['id_shop']);
            $resume = OrderSlip::getProductSlipResume($product['id_order_detail']);
            $product['quantity_refundable'] = $product['product_quantity'] - $resume['product_quantity'];
            $product['amount_refundable'] = $product['total_price_tax_excl'] - $resume['amount_tax_excl'];
            $product['amount_refundable_tax_incl'] = $product['total_price_tax_incl'] - $resume['amount_tax_incl'];
            $product['amount_refund'] = Tools::displayPrice($resume['amount_tax_incl'], $currency);
            $product['refund_history'] = OrderSlip::getProductSlipDetail($product['id_order_detail']);
            $product['return_history'] = OrderReturn::getProductReturnDetail($product['id_order_detail']);

            // if the current stock requires a warning
            if ($product['current_stock'] <= 0 && $display_out_of_stock_warning) {
                $this->displayWarning($this->l('This product is out of stock: ').' '.$product['product_name']);
            }
            if ($product['id_warehouse'] != 0) {
                $warehouse = new Warehouse((int)$product['id_warehouse']);
                $product['warehouse_name'] = $warehouse->name;
                $warehouse_location = WarehouseProductLocation::getProductLocation($product['product_id'], $product['product_attribute_id'], $product['id_warehouse']);
                if (!empty($warehouse_location)) {
                    $product['warehouse_location'] = $warehouse_location;
                } else {
                    $product['warehouse_location'] = false;
                }
            } else {
                $product['warehouse_name'] = '--';
                $product['warehouse_location'] = false;
            }
        }

        $gender = new Gender((int)$customer->id_gender, $this->context->language->id);

        $history = $order->getHistory($this->context->language->id);

        foreach ($history as &$order_state) {
            $order_state['text-color'] = Tools::getBrightness($order_state['color']) < 128 ? 'white' : 'black';
        }
        
        $crearGuia = 1;
        $guias = $this->dataContarGuias($order->id);
        
        if (sizeof($guias) && (int)$guias[0]["cnt"] > 0){
            $crearGuia = 0;
        }

        // Smarty assign
        $this->context->smarty->assign(array(
            'crear_guia' => $crearGuia,
        ));
        
        $this->tpl_view_vars = array(
            'order' => $order,
            'cart' => new Cart($order->id_cart),
            'customer' => $customer,
            'gender' => $gender,
            'customer_addresses' => $customer->getAddresses($this->context->language->id),
            'addresses' => array(
                'delivery' => $addressDelivery,
                'deliveryState' => isset($deliveryState) ? $deliveryState : null,
                'invoice' => $addressInvoice,
                'invoiceState' => isset($invoiceState) ? $invoiceState : null
            ),
            'customerStats' => $customer->getStats(),
            'products' => $products,
            'discounts' => $order->getCartRules(),
            'orders_total_paid_tax_incl' => $order->getOrdersTotalPaid(), // Get the sum of total_paid_tax_incl of the order with similar reference
            'total_paid' => $order->getTotalPaid(),
            'returns' => OrderReturn::getOrdersReturn($order->id_customer, $order->id),
            'customer_thread_message' => CustomerThread::getCustomerMessages($order->id_customer, null, $order->id),
            'orderMessages' => OrderMessage::getOrderMessages($order->id_lang),
            'messages' => Message::getMessagesByOrderId($order->id, true),
            'carrier' => new Carrier($order->id_carrier),
            'history' => $history,
            'states' => OrderState::getOrderStates($this->context->language->id),
            'warehouse_list' => $warehouse_list,
            'sources' => ConnectionsSource::getOrderSources($order->id),
            'currentState' => $order->getCurrentOrderState(),
            'currency' => new Currency($order->id_currency),
            'currencies' => Currency::getCurrenciesByIdShop($order->id_shop),
            'previousOrder' => $order->getPreviousOrderId(),
            'nextOrder' => $order->getNextOrderId(),
            'current_index' => self::$currentIndex,
            'carrierModuleCall' => $carrier_module_call,
            'iso_code_lang' => $this->context->language->iso_code,
            'id_lang' => $this->context->language->id,
            'can_edit' => ($this->tabAccess['edit'] == 1),
            'current_id_lang' => $this->context->language->id,
            'invoices_collection' => $order->getInvoicesCollection(),
            'not_paid_invoices_collection' => $order->getNotPaidInvoicesCollection(),
            'payment_methods' => $payment_methods,
            'invoice_management_active' => Configuration::get('PS_INVOICE', null, null, $order->id_shop),
            'display_warehouse' => (int)Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'),
            'HOOK_CONTENT_ORDER' => Hook::exec('displayAdminOrderContentOrder', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            'HOOK_CONTENT_SHIP' => Hook::exec('displayAdminOrderContentShip', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            'HOOK_TAB_ORDER' => Hook::exec('displayAdminOrderTabOrder', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            'HOOK_TAB_SHIP' => Hook::exec('displayAdminOrderTabShip', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
        );

        return parent::renderView();
    }
    
    public function postProcess(){
        if (Tools::isSubmit('submitAction')){
            
            $idOrden = "";
            if (Tools::isSubmit('id_order') && Tools::getValue('id_order') > 0) {
                $idOrden = (int)Tools::getValue('id_order');
            }
            
            switch (Tools::getValue('submitAction')){
                case 'crearGuia':
                    $this->crearGuia($idOrden);
                    break;
                    
                case 'imprimirGuia':
                    $this->imprimirGuia($idOrden);
                    break;
                    
                case 'imprimirRotulo':
                    $this->imprimirRotulo($idOrden);
                    break;
            }
        }
        
        parent::postProcess();
    }
    
    public function crearGuia($idOrden){
        ini_set('display_errors', 1);
        
        $order = new Order($idOrden);
        if (!Validate::isLoadedObject($order)) {
            $this->errors[] = Tools::displayError('The order cannot be found within your database.');
            return;
        }

        $customer = new Customer($order->id_customer);
        $carrier = new Carrier($order->id_carrier);
        $dirEntrega = new Address($order->id_address_delivery);
        $destino = "11001000";
        if (sizeof($dirEntrega) && !empty($dirEntrega->city)){
            list($destino) = explode('-', $dirEntrega->city);
        }
        
        $products = $this->getProducts($order);

        $url="http://sandbox.coordinadora.com/agw/ws/guias/1.4/server.php?wsdl";
        $cliente = new SoapClient($url, array("trace" => 1, "exception" => 0));

        $p = new stdClass();
        $p->codigo_remision = "";
        $p->fecha = date('Y-m-d');
        $p->id_cliente = 24074;
        $p->id_remitente = 0;
        $p->nombre_remitente =  "Happy Bear Tm S.A.S";
        $p->direccion_remitente = "Bogotá - Colombia";
        $p->telefono_remitente = "8282012 - 8282013";
        $p->ciudad_remitente = "11001000";
        $p->nit_destinatario = "";
        $p->div_destinatario = "";
        $p->nombre_destinatario =  $customer->firstname.' ' .$customer->lastname;
        $p->direccion_destinatario = $dirEntrega->address1.' '.$dirEntrega->address2;
        $p->ciudad_destinatario = $destino;
        $p->telefono_destinatario = !empty($dirEntrega->phone) ? $dirEntrega->phone : $dirEntrega->phone_mobile;
        $p->valor_declarado = $order->total_paid;
        $p->codigo_cuenta = 2;
        $p->codigo_producto = 0;
        $p->nivel_servicio = 1;
        $p->linea = "";
        $p->contenido = "Mercancia";
        $p->referencia = $order->id;
        $p->observaciones = "";
        $p->estado = "IMPRESO";
        $p->detalle = array();

        foreach ($products as $key => $prod){
            $producto = new Product($prod["product_id"]);
            
            $pdt = new stdClass();
            $pdt->ubl = "0";
            $pdt->alto = (float)$producto->height > 0 ? $producto->height : 2;
            $pdt->ancho = (float)$producto->depth > 0 ? $producto->depth : 31;
            $pdt->largo = (float)$producto->width > 0 ? $producto->width : 42;
            
            $pdt->peso = (int)$prod["product_quantity"] * $producto->weight;
            if ($pdt->peso <= 0){
                $pdt->peso = 1;
            }
            
            $pdt->unidades = $prod["product_quantity"];
            $pdt->referencia = $prod["product_id"]."-".$order->id;
            $pdt->nombre_empaque = "";
            
            $p->detalle[] = $pdt;
        }

        $p->cuenta_contable = "";
        $p->centro_costos = "";
        $p->recaudos = "";
        $p->margen_izquierdo = "3";
        $p->margen_superior = "1";
        $p->id_rotulo = 2;
        $p->formato_impresion = 0;
        $p->usuario_vmi = "";
        $p->atributo1_nombre = "";
        $p->atributo1_valor = "";
        $p->tipo_notificacion = "";
        $p->destino_notificacion  =  "";
        $p->usuario = "happybear.ws";	
        $p->clave = "161bb85251f69b2a84548d0ec83dec58371e6e8b1d3515cdfd0f90a0b078bf11";

        try{
            $res = $cliente->Guias_generarGuia($p);
            $this->dataGuardarGuia($order->id, $res->id_remision, $res->codigo_remision);
            
            $dirs = array("storage", "guias");
            $dir = _PS_ROOT_DIR_;
            
            foreach ($dirs as $d){
                $dir .= "/".$d;
                if (!is_dir($dir)){
                    @mkdir($dir);
                }
            }
            
            file_put_contents($dir."/guia_".$order->id."_".$res->id_remision.".pdf", $res->pdf_guia);
            file_put_contents($dir."/rotulo_".$order->id."_".$res->id_remision.".pdf", $res->pdf_rotulo);
            
            $this->displayInformation($this->l('Se ha creado la guía exitosamente.'));
            
        }
        catch(Exception $ex){
            $this->errors[] = Tools::displayError($ex->getMessage());
        }
    }
    
    public function imprimirGuia($idOrden){
        $order = new Order($idOrden);
        if (!Validate::isLoadedObject($order)) {
            $this->errors[] = Tools::displayError('The order cannot be found within your database.');
            return;
        }
        
        $guias = $this->dataObtenerGuias($idOrden);
        if (!sizeof($guias)){
            $this->errors[] = Tools::displayError('La orden no tiene guías asignadas.');
            return;
        }
        
        $file = _PS_ROOT_DIR_."/storage/guias/guia_".$order->id."_".$guias[0]["id_remision"].".pdf";
        
        if (!is_file($file)){
            $this->errors[] = Tools::displayError('El archivo de la guía no se ha generado.');
            return;
        }
        
        $contenido = file_get_contents($file);
        header('Content-type: application/pdf');
		echo base64_decode($contenido);
    }
    
    public function imprimirRotulo($idOrden){
        $order = new Order($idOrden);
        if (!Validate::isLoadedObject($order)) {
            $this->errors[] = Tools::displayError('The order cannot be found within your database.');
            return;
        }
        
        $guias = $this->dataObtenerGuias($idOrden);
        if (!sizeof($guias)){
            $this->errors[] = Tools::displayError('La orden no tiene guías asignadas.');
            return;
        }
        
        $file = _PS_ROOT_DIR_."/storage/guias/rotulo_".$order->id."_".$guias[0]["id_remision"].".pdf";
        
        if (!is_file($file)){
            $this->errors[] = Tools::displayError('El archivo del rótulo no se ha generado.');
            return;
        }
        
        $contenido = file_get_contents($file);
        header('Content-type: application/pdf');
		echo base64_decode($contenido);
    }
    
    public function dataContarGuias($idOrden){
        $db = Db::getInstance();
        $sql = "select count(*) cnt from " . _DB_PREFIX_ . "order_guia where id_order = ".$idOrden;
        return $db->executeS($sql);
    }
    
    public function dataObtenerGuias($idOrden){
        $db = Db::getInstance();
        $sql = "select * from " . _DB_PREFIX_ . "order_guia where id_order = ".$idOrden;
        return $db->executeS($sql);
    }
    
    public function dataGuardarGuia($idOrden, $idRemision, $codigoRemision){
        $db = Db::getInstance();
        $sql = "insert into " . _DB_PREFIX_ . "order_guia (id_order, id_remision, cod_remision) values ('".$idOrden."', '".$idRemision."', '".$codigoRemision."')";
        return $db->execute($sql);
    }
}
