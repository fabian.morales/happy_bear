(function ($, window) {
    function obtenerCiudades() {
        $.ajax({
            url: 'index.php?controller=custom&tarea=obtener_ciudades',
            data: {id_depto: $("#id_state").val()},
            method: 'post',
            dataType: 'json',
            success: function (json) {
                $("#city").val('');
                $("#city").html('');

                $.each(json, function (i, o) {
                    var $selected = '';

                    if ((o.codigo + '-' + o.nombre) === $("#city_old").val()) {
                        $selected = ' selected="selected"';
                    }
                    
                    $("#city").append('<option value="' + o.codigo + '-' + o.nombre + '"' + $selected + '>' + o.codigo + '-' + o.nombre + '</option>');

                    if ((o.codigo + '-' + o.nombre) === $("#city_old").val()) {
                        $("#city").val(o.codigo + '-' + o.nombre);
                    }

                    $.uniform.update("#city");
                });
            }
        });
    }

    $(document).ready(function () {
        $("form#add_address #id_state").change(function (e) {
            e.preventDefault();
            obtenerCiudades();
        });

        if ($("form#add_address #id_state").size() > 0){
            obtenerCiudades();
        }
    });
})(jQuery, window);